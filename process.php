<?php
//setting message types for both save and delete button
//starting a session
session_start();
//connecting to database
$mysqli =  new mysqli('localhost', 'root', '','crud') or die (mysqli_error($mysqli));
//setting name and location to empty strings so as to check whether edit button has been clicked
//and if the id is accessed when editing and updating a record
$id = 0;
$update = false;
$name = "";
$location = "";
$image_test = "";
//check if save button is pressed
//access using global variable $_POST
//the isset function checks if a condition has been met
//save is the name of the button
if (isset($_POST['save'])){
    //this is the path that will store our image
    $target = "pictures/".basename($_FILES['pimage']['name']);
    //store name and location in variables
    $name = $_POST['name'];
    $location = $_POST['location'];
    $image_test = $_FILES['pimage']['name'];
    //storing sql query in a variable
    $insert = "INSERT INTO data (name, location,image)VALUES ('$name','$location','$image_test')";
    //placing my image to a folder in my server so that i can have a path to retrieve it from
    if (mysqli_query($mysqli,$insert)){
        move_uploaded_file($_FILES['pimage']['tmp_name'],$target);
        echo "<script>alert('image has been uploaded to folder')</script>";
    } else {
        echo "<script>alert('error in uploading file')</script>";
    }
    //inserting data when button save is pressed
//    $mysqli->query("INSERT INTO data (name, location,image)VALUES ('$name','$location','$image_test')")
//    or die($mysqli->error);
    //setting message and message type for the save button
    $_SESSION['message'] = "Record has been saved!";
    $_SESSION['msg_type'] = "success";
    //redirecting user back to index.php
    header("location: index.php");

}
//deleting a record
//first check if delete button is clicked
if (isset($_GET['delete'])){
    //deleting record using the id
    $id = $_GET['delete'];
    //SQL Query for fetching the id for the row deleted
    $mysqli->query("DELETE FROM data WHERE id=$id") or die($mysqli->error);
    //setting message and message type for the save button
    $_SESSION['message'] = "Record has been deleted!";
    $_SESSION['msg_type'] = "danger";
    //redirecting user back to index.php
    header("location: index.php");


}
//checking if edit button is clicked
if (isset($_GET['edit'])){
    $id = $_GET['edit'];
    //variable update
    $update = true;
    //pulling requested record
    $result = $mysqli->query("SELECT * FROM data WHERE id=$id") or die($mysqli->error);
    //check if record exists
        $row = $result->fetch_array();
        $name = $row['name'];
        $location = $row['location'];

}

//updating and editing records
//checking if update button is clicked
if (isset($_POST['update'])){
    //setting variables to store the newly edited data
    $id = $_POST['id'];
    $name = $_POST['name'];
    $location = $_POST['location'];
    //query to update
    $mysqli->query("UPDATE data SET name='$name', location='$location' , image='$image_test' WHERE id=$id") or die($mysqli->error);
    //setting session messages for updates
    $_SESSION['message'] = "Record has been updated!";
    $_SESSION['msg_type'] = "warning";

    header("location: index.php");

}



























