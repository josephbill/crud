<html>
<head>
    <title>PHP CRUD</title>
    <link rel="stylesheet" href="bootstrap-3.4.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.4.1-dist/css/bootstrap-theme.css">
    <meta charset="UTF-8">>
</head>
<body>
<?php require_once 'process.php'?>
<!--displaying our session manager messages-->
<?php
 if (isset($_SESSION['message'])):
?>
<!--the div will use the bootstrap class alert and set the alert type
using the msg_type from the sessions in process.php-->
 <div class="container container-fluid">
     <div class="alert alert-<?=$_SESSION['msg_type']?>">
         <!--printing out our alert message using PHP-->
         <?
         echo $_SESSION['message'];
         unset($_SESSION['message']);
         ?>
     </div>
     <?php endif ?>

 </div>

<!-- method to fetch our data from the database-->
<?php
  //connect to mysql db
  $mysqli = new mysqli('localhost','root','','crud')or die(mysqli_error($mysqli));
  $result = $mysqli->query("SELECT * FROM data") or die($mysqli->error);
//print out the outputs from the function below
//pre_r($result);
//now we use the fetch assoc() function to pull our records
//pre_r($result->fetch_assoc());
//this method will print records when they
// are output if the function is repeated : we prevent this code repetition using a while loop
 //the function below allows u to see what records are accessed , number of rows and columns
?>

<!--replace with code in empty file ioi-->
<!--  creating table to display our data -->
<div class="container container-fluid">
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Location</th>
            <th colspan="2">Action</th>
        </tr>
        </thead>




        <!--     now we create our loop to pull our records-->
        <?php
        while($row = $result->fetch_assoc()):
            ?>
            <!--     displaying the result in a table cell-->
            <tr>
                <td><?php echo $row['name'];?></td>
                <td><?php echo $row['location'];?></td>
                <td>
                    <?php
                    echo "<div id='img_div'>";
                    echo "<img style='width: 100px; height: 100px;' src='pictures/".$row['image']."'>";
                    echo "</div>";
                    ?>

                </td>
                <td>
                    <!--             the edit link-->
                    <a href="index.php?edit=<?php echo $row['id'];?> " class="btn btn-info" >Edit</a>
                    <!--             the delete link-->
                    <a href="process.php?delete=<?php echo $row['id'];?>" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            <!--     ending the while loop-->
        <?php endwhile;
        ?>
    </table>
</div>


<?php
//function is used in the php script above
  function pre_r($array){
      echo '<pre>';
      print_r($array);
      echo '<pre>';
  }


?>




<div class="container container-fluid">
    <form action="process.php" method="POST" enctype="multipart/form-data">
<!--        hiding the id field so that the update can happen using the POST method-->
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="form-group"> <label for="name">Name</label>
            <input class="form-control" type="text" name="name" value="<?php echo $name;?>" placeholder="enter your name">
        </div>
        <div class="form-group">
            <label for="name">Location</label>
            <input  class="form-control" type="text" name="location" value="<?php echo $location;?>" placeholder="enter your location">
        </div>
        <div class="form-group">
            <label for="name">Image</label>
            <input  class="form-control" type="file" name="pimage"  value="<?php echo $image_test ?>" id="image">
        </div>
        <div class="form-group">
<!--            checking if btn update has been clicked-->
            <?php
            if ($update == true):
            ?>
            <button type="submit" class="btn btn-warning" name="update">Update</button>
            <?php else: ?>
            <button class="btn btn-primary" type="submit" name="save">Save</button>
            <?php endif; ?>
        </div>
    </form>

</div>

</body>

</html>